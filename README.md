## GNSS Tools

Tools to convert raw GNSS files to RINEX including:

- Convert raw GNSS files to Rinex:
 - Currently only works with Trimble T02 files using runpkr

- Create Meterological record from series of RINEX meterological files 

---